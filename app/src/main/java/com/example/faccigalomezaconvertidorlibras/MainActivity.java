package com.example.faccigalomezaconvertidorlibras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView Ingresevalor;
    Button convertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Ingresevalor = (TextView) findViewById(R.id.editTextIngreseValor);
        convertir= (Button) findViewById(R.id.buttonConvertir);

        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Activity_resultado.class);

                Bundle bundle = new Bundle();
                bundle.putString("dato", Ingresevalor.getText().toString());



                intent.putExtras(bundle);
                startActivity(intent);

                Ingresevalor.setText("");
            }
        });
    }
}
